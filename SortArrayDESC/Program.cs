﻿namespace SortArrayDESC;

public class Program
{
    static void Main(string[] args)
    {
        var sorted = SortArrayDesc(new int[] { 1 });
        Console.WriteLine(sorted);
    }

    public static int[] SortArrayDesc(int[] x)
    {
        //simple sorting algoritm
        for (int i = 0; i < x.Length - 1; i++)
        {
            for (int j = i; j < x.Length; j++)
            {
                if (x[i] < x[j])
                {
                    int t = x[i];
                    x[i] = x[j];
                    x[j] = t;
                }
            }
        }

        return x;
    }

}