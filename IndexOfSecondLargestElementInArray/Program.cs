﻿namespace IndexOfSecondLargestElementInArray;

public class Program
{
    static void Main(string[] args)
    {
        var myArr = new[] { 1, 2, 3 };
        var result = IndexOfSecondLargestElementInArray(myArr);
        Console.WriteLine(result);
    }

    public static int IndexOfSecondLargestElementInArray(int[] x)
    {
        int result = -1;
        // kết quả hàm sau khi thực thi là vị trí(index) của số lớn thứ 2 trong mảng
        if (x.Length < 2) return result;
        int max_1 = x[0];
        int maxp_1 = 0;
        int maxp_2 = 0;
        int? max_2 = null;
        // case 1 max1 < x[i] max2 < max1
        // case 2 max1 == x[i] max2<max1
        // case 3 max2 < x[i] max1 != x[i]
        // case 4 max2 == x[i] max1 != x[i]
        for (int i = 1; i < x.Length; i++)
        {

            if (max_1 < x[i])
            {
                if (max_2 != max_1)
                {
                    max_2 = max_1;
                    maxp_2 = maxp_1;
                }
                max_1 = x[i];
                maxp_1 = i;
            }
            else
            if (max_1 == x[i] && max_1 != max_2)
            {
                max_2 = max_1;
                maxp_2 = i;
            }
            else
            if (max_2 < x[i] && max_1 > x[i] || max_2 == null)
            {
                max_2 = x[i];
                maxp_2 = i;
            }

        }
        return maxp_2;
    }
}